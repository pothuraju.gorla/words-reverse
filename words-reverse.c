

/**
 * C application that accepts filename from command line 
 * and creates copy of file with each word reversed.
 */

#include<stdio.h>
#include<string.h>

void
reverseString (char *str)
{
  int l, i;
  char *begin_ptr, *end_ptr, ch;
  
  l = strlen (str);

  begin_ptr = str;
  end_ptr = str;

  for (i = 0; i < l - 1; i++)
    end_ptr++;

  for (i = 0; i < l / 2; i++)
    {

      ch = *end_ptr;
      *end_ptr = *begin_ptr;
      *begin_ptr = ch;

      begin_ptr++;
      end_ptr--;
    }
}

int
main (int argc, char *argv[])
{


  int pos = 0, i, ret;
  char ch, line[500], bb[1000] = "";

  FILE *fp = fopen (argv[1], "r");
  FILE *fp2 = fopen ("output.txt", "w+");

  while (ret = fscanf (fp, "%s", line))	
    {
      if (ret == EOF)
	{
	  break;
	}
      strcat (bb, line);
      strcat (bb, "\n");
    }

  char *tok = strtok (bb, "\n +=");
  while (tok != NULL)
    {
      reverseString (tok);
      fputs (tok, fp2);
      fputs (" ", fp2);
      tok = strtok (NULL, "\n ,+=");
    }


  fclose (fp);
  fclose (fp2);

  return 0;
}
